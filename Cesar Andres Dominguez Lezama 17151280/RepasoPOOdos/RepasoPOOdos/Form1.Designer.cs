﻿
namespace RepasoPOOdos
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TBNombre = new System.Windows.Forms.TextBox();
            this.TBNumDeCuenta = new System.Windows.Forms.TextBox();
            this.NUDSaldoInicial = new System.Windows.Forms.NumericUpDown();
            this.BAceptar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.NUDCantidadMovimiento = new System.Windows.Forms.NumericUpDown();
            this.TBConceptoMovimiento = new System.Windows.Forms.TextBox();
            this.BDeposito = new System.Windows.Forms.Button();
            this.BRetiro = new System.Windows.Forms.Button();
            this.RTBMovimientos = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bEliminar = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.BModificar = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.NUDSaldoInicial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDCantidadMovimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(76, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(76, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Numero Cuenta";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(76, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Saldo Inicial";
            // 
            // TBNombre
            // 
            this.TBNombre.Location = new System.Drawing.Point(126, 43);
            this.TBNombre.Name = "TBNombre";
            this.TBNombre.Size = new System.Drawing.Size(253, 20);
            this.TBNombre.TabIndex = 3;
            // 
            // TBNumDeCuenta
            // 
            this.TBNumDeCuenta.Location = new System.Drawing.Point(163, 91);
            this.TBNumDeCuenta.Name = "TBNumDeCuenta";
            this.TBNumDeCuenta.Size = new System.Drawing.Size(216, 20);
            this.TBNumDeCuenta.TabIndex = 4;
            // 
            // NUDSaldoInicial
            // 
            this.NUDSaldoInicial.Location = new System.Drawing.Point(146, 136);
            this.NUDSaldoInicial.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.NUDSaldoInicial.Name = "NUDSaldoInicial";
            this.NUDSaldoInicial.Size = new System.Drawing.Size(110, 20);
            this.NUDSaldoInicial.TabIndex = 5;
            // 
            // BAceptar
            // 
            this.BAceptar.ForeColor = System.Drawing.Color.DarkGreen;
            this.BAceptar.Location = new System.Drawing.Point(273, 133);
            this.BAceptar.Name = "BAceptar";
            this.BAceptar.Size = new System.Drawing.Size(106, 23);
            this.BAceptar.TabIndex = 6;
            this.BAceptar.Text = "Crear Cuenta";
            this.BAceptar.UseVisualStyleBackColor = true;
            this.BAceptar.Click += new System.EventHandler(this.BAceptar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(79, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Cantidad";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(79, 246);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Concepto";
            // 
            // NUDCantidadMovimiento
            // 
            this.NUDCantidadMovimiento.Enabled = false;
            this.NUDCantidadMovimiento.Location = new System.Drawing.Point(136, 200);
            this.NUDCantidadMovimiento.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.NUDCantidadMovimiento.Name = "NUDCantidadMovimiento";
            this.NUDCantidadMovimiento.Size = new System.Drawing.Size(120, 20);
            this.NUDCantidadMovimiento.TabIndex = 9;
            // 
            // TBConceptoMovimiento
            // 
            this.TBConceptoMovimiento.Enabled = false;
            this.TBConceptoMovimiento.Location = new System.Drawing.Point(136, 246);
            this.TBConceptoMovimiento.Name = "TBConceptoMovimiento";
            this.TBConceptoMovimiento.Size = new System.Drawing.Size(120, 20);
            this.TBConceptoMovimiento.TabIndex = 10;
            // 
            // BDeposito
            // 
            this.BDeposito.Enabled = false;
            this.BDeposito.Location = new System.Drawing.Point(273, 197);
            this.BDeposito.Name = "BDeposito";
            this.BDeposito.Size = new System.Drawing.Size(106, 23);
            this.BDeposito.TabIndex = 11;
            this.BDeposito.Text = "Depositar";
            this.BDeposito.UseVisualStyleBackColor = true;
            this.BDeposito.Click += new System.EventHandler(this.BDeposito_Click);
            // 
            // BRetiro
            // 
            this.BRetiro.Enabled = false;
            this.BRetiro.Location = new System.Drawing.Point(385, 197);
            this.BRetiro.Name = "BRetiro";
            this.BRetiro.Size = new System.Drawing.Size(106, 23);
            this.BRetiro.TabIndex = 12;
            this.BRetiro.Text = "Retiro";
            this.BRetiro.UseVisualStyleBackColor = true;
            this.BRetiro.Click += new System.EventHandler(this.BRetiro_Click);
            // 
            // RTBMovimientos
            // 
            this.RTBMovimientos.Enabled = false;
            this.RTBMovimientos.Location = new System.Drawing.Point(598, 310);
            this.RTBMovimientos.Name = "RTBMovimientos";
            this.RTBMovimientos.Size = new System.Drawing.Size(237, 96);
            this.RTBMovimientos.TabIndex = 13;
            this.RTBMovimientos.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(595, 294);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Movimientos:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(171, 294);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Datos:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dataGridView1.Enabled = false;
            this.dataGridView1.Location = new System.Drawing.Point(174, 310);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(401, 150);
            this.dataGridView1.TabIndex = 16;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseClick);
            this.dataGridView1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseDoubleClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Nombre";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 150;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Numero de Cuenta";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 150;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Saldo";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // bEliminar
            // 
            this.bEliminar.BackColor = System.Drawing.SystemColors.Control;
            this.bEliminar.Enabled = false;
            this.bEliminar.ForeColor = System.Drawing.Color.Red;
            this.bEliminar.Location = new System.Drawing.Point(64, 407);
            this.bEliminar.Name = "bEliminar";
            this.bEliminar.Size = new System.Drawing.Size(93, 31);
            this.bEliminar.TabIndex = 18;
            this.bEliminar.Text = "Eliminar";
            this.bEliminar.UseVisualStyleBackColor = false;
            this.bEliminar.Click += new System.EventHandler(this.button2_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 325);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(157, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "(Doble clic para seleccionar fila)";
            // 
            // BModificar
            // 
            this.BModificar.Enabled = false;
            this.BModificar.ForeColor = System.Drawing.Color.DarkBlue;
            this.BModificar.Location = new System.Drawing.Point(64, 359);
            this.BModificar.Name = "BModificar";
            this.BModificar.Size = new System.Drawing.Size(93, 31);
            this.BModificar.TabIndex = 22;
            this.BModificar.Text = "Modificar";
            this.BModificar.UseVisualStyleBackColor = true;
            this.BModificar.Click += new System.EventHandler(this.BModificar_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Impact", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(222, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(220, 29);
            this.label9.TabIndex = 23;
            this.label9.Text = "Cuenta Bancaria v2.0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(448, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(166, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "Cesar Andres Dominguez Lezama";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(855, 450);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.BModificar);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.bEliminar);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.RTBMovimientos);
            this.Controls.Add(this.BRetiro);
            this.Controls.Add(this.BDeposito);
            this.Controls.Add(this.TBConceptoMovimiento);
            this.Controls.Add(this.NUDCantidadMovimiento);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.BAceptar);
            this.Controls.Add(this.NUDSaldoInicial);
            this.Controls.Add(this.TBNumDeCuenta);
            this.Controls.Add(this.TBNombre);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.NUDSaldoInicial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDCantidadMovimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TBNombre;
        private System.Windows.Forms.TextBox TBNumDeCuenta;
        private System.Windows.Forms.NumericUpDown NUDSaldoInicial;
        private System.Windows.Forms.Button BAceptar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown NUDCantidadMovimiento;
        private System.Windows.Forms.TextBox TBConceptoMovimiento;
        private System.Windows.Forms.Button BDeposito;
        private System.Windows.Forms.Button BRetiro;
        private System.Windows.Forms.RichTextBox RTBMovimientos;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button bEliminar;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button BModificar;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
    }
}

