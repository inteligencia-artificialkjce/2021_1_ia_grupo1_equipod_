﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RepasoPOOdos
{
    public partial class Form1 : Form
    {
        CuentaBancaria Cuenta;
        public Form1()
        {
            InitializeComponent();
        }

        private void BAceptar_Click(object sender, EventArgs e)
        {      

            if (TBNombre.Text == "" || TBNumDeCuenta.Text == "" || NUDSaldoInicial.Text=="")
            {
                MessageBox.Show("LLene todos los campos");
            }
            else
            {
                Cuenta = new CuentaBancaria(TBNombre.Text, TBNumDeCuenta.Text, (double)NUDSaldoInicial.Value);
                ActualizarInformacionPresentada();
                BDeposito.Enabled = true;
                BRetiro.Enabled = true;
                TBConceptoMovimiento.Enabled = true;
                NUDCantidadMovimiento.Enabled = true;
                dataGridView1.Enabled = true;
                RTBMovimientos.Enabled = true;
                BModificar.Enabled = false;
                bEliminar.Enabled = true;

                DataGridViewRow fila = new DataGridViewRow();
                fila.CreateCells(dataGridView1);
                fila.Cells[0].Value = TBNombre.Text;
                fila.Cells[1].Value = TBNumDeCuenta.Text;
                fila.Cells[2].Value = NUDSaldoInicial.Value;
                dataGridView1.Rows.Add(fila);

                TBNombre.Text = "";
                TBNumDeCuenta.Text = "";
                NUDSaldoInicial.Text = "0";
            }

        }

        private void BDeposito_Click(object sender, EventArgs e)
        {

            if (NUDCantidadMovimiento.Text == "" || TBConceptoMovimiento.Text == "")
            {
                MessageBox.Show("LLene todos los campos");
            }
            else
            {

            Cuenta.Depositar((double)NUDCantidadMovimiento.Value, TBConceptoMovimiento.Text);
            ActualizarInformacionPresentada();
            TBConceptoMovimiento.Text = "";
            string x = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].Value.ToString();
            string y = NUDCantidadMovimiento.Text;

            DataGridViewRow fila = new DataGridViewRow();
            fila.CreateCells(dataGridView1);
            fila.Cells[2].Value = x;          

            int total;
            total = int.Parse(x) + int.Parse(y);
            
            int poc = dataGridView1.CurrentRow.Index;
            dataGridView1[2, poc].Value = total;

            NUDCantidadMovimiento.Text = "0";
            TBConceptoMovimiento.Text = "";

            }
        }

        private void BRetiro_Click(object sender, EventArgs e)
        {
            if (NUDCantidadMovimiento.Text == "" || TBConceptoMovimiento.Text == "")
            {
                MessageBox.Show("LLeno todos los campos");
            }
            else
            {
                Cuenta.Retirar((double)NUDCantidadMovimiento.Value, TBConceptoMovimiento.Text);
                ActualizarInformacionPresentada();
                TBConceptoMovimiento.Text = "";

                string x = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].Value.ToString();
                string y = NUDCantidadMovimiento.Text;

                DataGridViewRow fila = new DataGridViewRow();
                fila.CreateCells(dataGridView1);
                fila.Cells[2].Value = x;

                int total;
                total = int.Parse(x) - int.Parse(y);

                int poc = dataGridView1.CurrentRow.Index;
                dataGridView1[2, poc].Value = total;

                NUDCantidadMovimiento.Text = "0";
                TBConceptoMovimiento.Text = "";
            }
        }

        private void ActualizarInformacionPresentada()
        {
            RTBMovimientos.Text = Cuenta.GetDatosMovimientos();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {


        }

        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            string a = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
            string b = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1].Value.ToString();
            string c = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].Value.ToString();
            TBNombre.Text = a;
            TBNumDeCuenta.Text = b;
            NUDSaldoInicial.Value = int.Parse(c);
            BModificar.Enabled = true;

        }

        private void BModificar_Click(object sender, EventArgs e)
        {
            int poc = dataGridView1.CurrentRow.Index;
            dataGridView1[0, poc].Value = TBNombre.Text;
            dataGridView1[1, poc].Value = TBNumDeCuenta.Text;
            dataGridView1[2, poc].Value = NUDSaldoInicial.Value;

            BModificar.Enabled = false;
        }
    }
}
