﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RepasoPOO
{
    public partial class Form1 : Form
    {
        CuentaBancaria Cuenta;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cuenta = new CuentaBancaria(TBNombre.Text, TBNumDeCuenta.Text,TBCantidad.Text);
            TBNombre.Text = textBox1.Text;
            TBNumDeCuenta.Text = textBox2.Text;
            NUDSaldo.Text = textBox3.Text;
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";

        }

        private void button3_Click(object sender, EventArgs e)
        {
            int numero2 = int.Parse(TBCantidad.Text);
            int numero1 = int.Parse(NUDSaldo.Text);
            int total = numero1 - numero2;

            if (total>=0)
            {
                NUDSaldo.Text = total.ToString();
            }
            else
            {
                MessageBox.Show("Saldo Insuficiente");
            }
            
        }

        private void BDepositar_Click(object sender, EventArgs e)
        {
            int numero1 = int.Parse(TBCantidad.Text);
            int numero2 = int.Parse(NUDSaldo.Text);
            int total = numero1 + numero2;
            NUDSaldo.Text = total.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
