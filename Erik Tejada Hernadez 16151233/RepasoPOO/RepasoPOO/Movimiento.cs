﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepasoPOO
{
    class Movimiento
    {
        double Monto;
        string Concepto;
        int Tipo;

        public Movimiento(double Monto, string Concepto, int Tipo)
        {
            this.Monto = Monto;
            this.Concepto = Concepto;
            this.Tipo = Tipo;
        }

        public string GetDatos()
        {
            string datos = "";
            datos += Monto + " - ";
            datos += Concepto + " - ";
            datos += Tipo + " - ";
            return datos;

        }

        public double GetMonto()
        {
            return Monto;
        }

        public int GetTipo()
        {

        }
    }
}
