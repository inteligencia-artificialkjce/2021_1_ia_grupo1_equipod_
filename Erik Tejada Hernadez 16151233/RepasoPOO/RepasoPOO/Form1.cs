﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RepasoPOO
{
    public partial class FDatosCuenta : Form
    {
        CuentaBancaria Cuenta;

        public FDatosCuenta()
        {
            InitializeComponent();
        }

        private void BAceptar_Click(object sender, EventArgs e)
        {
            Cuenta = new CuentaBancaria(TBNombre.Text, TBNumCta.Text, (double)NUDSaldoInicial.Value);
            ActualizarInformacion();
            BDeposito.Enabled = true;
            BRetiro.Enabled = true;
            TBConcepto.Enabled = true;
            NUDCantidadMovimiento.Enabled = true;
        }

        private void BRetiro_Click(object sender, EventArgs e)
        {
            Cuenta.Retirar((double)NUDCantidadMovimiento.Value, TBConcepto.Text);
            ActualizarInformacion();

        }

        private void BDeposito_Click(object sender, EventArgs e)
        {
            Cuenta.Depositar((double)NUDCantidadMovimiento.Value, TBConcepto.Text);
            ActualizarInformacion();

        }

        public void ActualizarInformacion()
        {
            RMovimientos.Text = Cuenta.GetDatosMovimientos();
            LSaldo.Text = "Saldo: $" + Convert.ToString(Cuenta.GetSaldo());
            NUDCantidadMovimiento.Value = 0;
            TBConcepto.Text = "";
        }
    }
}
