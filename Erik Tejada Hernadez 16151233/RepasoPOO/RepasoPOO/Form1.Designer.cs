﻿namespace RepasoPOO
{
    partial class FDatosCuenta
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TBNombre = new System.Windows.Forms.TextBox();
            this.TBNumCta = new System.Windows.Forms.TextBox();
            this.NUDSaldoInicial = new System.Windows.Forms.NumericUpDown();
            this.BAceptar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.BDeposito = new System.Windows.Forms.Button();
            this.BRetiro = new System.Windows.Forms.Button();
            this.NUDCantidadMovimiento = new System.Windows.Forms.NumericUpDown();
            this.TBConcepto = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.RMovimientos = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.LSaldo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.NUDSaldoInicial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDCantidadMovimiento)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Numero de Cuenta:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Saldo:";
            // 
            // TBNombre
            // 
            this.TBNombre.Location = new System.Drawing.Point(216, 6);
            this.TBNombre.Name = "TBNombre";
            this.TBNombre.Size = new System.Drawing.Size(197, 22);
            this.TBNombre.TabIndex = 3;
            // 
            // TBNumCta
            // 
            this.TBNumCta.Location = new System.Drawing.Point(216, 53);
            this.TBNumCta.Name = "TBNumCta";
            this.TBNumCta.Size = new System.Drawing.Size(197, 22);
            this.TBNumCta.TabIndex = 4;
            // 
            // NUDSaldoInicial
            // 
            this.NUDSaldoInicial.Location = new System.Drawing.Point(154, 100);
            this.NUDSaldoInicial.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.NUDSaldoInicial.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.NUDSaldoInicial.Name = "NUDSaldoInicial";
            this.NUDSaldoInicial.Size = new System.Drawing.Size(120, 22);
            this.NUDSaldoInicial.TabIndex = 5;
            this.NUDSaldoInicial.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // BAceptar
            // 
            this.BAceptar.Location = new System.Drawing.Point(289, 90);
            this.BAceptar.Name = "BAceptar";
            this.BAceptar.Size = new System.Drawing.Size(124, 32);
            this.BAceptar.TabIndex = 6;
            this.BAceptar.Text = "Crear Cuenta";
            this.BAceptar.UseVisualStyleBackColor = true;
            this.BAceptar.Click += new System.EventHandler(this.BAceptar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Cantidad:";
            // 
            // BDeposito
            // 
            this.BDeposito.Enabled = false;
            this.BDeposito.Location = new System.Drawing.Point(442, 233);
            this.BDeposito.Name = "BDeposito";
            this.BDeposito.Size = new System.Drawing.Size(87, 34);
            this.BDeposito.TabIndex = 9;
            this.BDeposito.Text = "Depósito";
            this.BDeposito.UseVisualStyleBackColor = true;
            this.BDeposito.Click += new System.EventHandler(this.BDeposito_Click);
            // 
            // BRetiro
            // 
            this.BRetiro.Enabled = false;
            this.BRetiro.Location = new System.Drawing.Point(442, 278);
            this.BRetiro.Name = "BRetiro";
            this.BRetiro.Size = new System.Drawing.Size(87, 34);
            this.BRetiro.TabIndex = 10;
            this.BRetiro.Text = "Retiro";
            this.BRetiro.UseVisualStyleBackColor = true;
            this.BRetiro.Click += new System.EventHandler(this.BRetiro_Click);
            // 
            // NUDCantidadMovimiento
            // 
            this.NUDCantidadMovimiento.Enabled = false;
            this.NUDCantidadMovimiento.Location = new System.Drawing.Point(154, 200);
            this.NUDCantidadMovimiento.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.NUDCantidadMovimiento.Name = "NUDCantidadMovimiento";
            this.NUDCantidadMovimiento.Size = new System.Drawing.Size(120, 22);
            this.NUDCantidadMovimiento.TabIndex = 11;
            // 
            // TBConcepto
            // 
            this.TBConcepto.Enabled = false;
            this.TBConcepto.Location = new System.Drawing.Point(154, 239);
            this.TBConcepto.Name = "TBConcepto";
            this.TBConcepto.Size = new System.Drawing.Size(182, 22);
            this.TBConcepto.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 244);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 17);
            this.label5.TabIndex = 13;
            this.label5.Text = "Concepto:";
            // 
            // RMovimientos
            // 
            this.RMovimientos.Location = new System.Drawing.Point(37, 323);
            this.RMovimientos.Name = "RMovimientos";
            this.RMovimientos.Size = new System.Drawing.Size(492, 168);
            this.RMovimientos.TabIndex = 14;
            this.RMovimientos.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 295);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = "Movimientos:";
            // 
            // LSaldo
            // 
            this.LSaldo.AutoSize = true;
            this.LSaldo.Location = new System.Drawing.Point(414, 509);
            this.LSaldo.Name = "LSaldo";
            this.LSaldo.Size = new System.Drawing.Size(48, 17);
            this.LSaldo.TabIndex = 16;
            this.LSaldo.Text = "Saldo:";
            // 
            // FDatosCuenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 553);
            this.Controls.Add(this.LSaldo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.RMovimientos);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TBConcepto);
            this.Controls.Add(this.NUDCantidadMovimiento);
            this.Controls.Add(this.BRetiro);
            this.Controls.Add(this.BDeposito);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.BAceptar);
            this.Controls.Add(this.NUDSaldoInicial);
            this.Controls.Add(this.TBNumCta);
            this.Controls.Add(this.TBNombre);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FDatosCuenta";
            this.Text = "Datos Cuenta";
            ((System.ComponentModel.ISupportInitialize)(this.NUDSaldoInicial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDCantidadMovimiento)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TBNombre;
        private System.Windows.Forms.TextBox TBNumCta;
        private System.Windows.Forms.NumericUpDown NUDSaldoInicial;
        private System.Windows.Forms.Button BAceptar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button BDeposito;
        private System.Windows.Forms.Button BRetiro;
        private System.Windows.Forms.NumericUpDown NUDCantidadMovimiento;
        private System.Windows.Forms.TextBox TBConcepto;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox RMovimientos;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label LSaldo;
    }
}

