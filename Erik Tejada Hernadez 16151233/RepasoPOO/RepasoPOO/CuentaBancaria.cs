﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepasoPOO
{
    class CuentaBancaria
    {
        string NombreCliente;
        string NumeroCuenta;
        double Saldo;
        List<Movimiento> Movimientos;

        public CuentaBancaria(string NombreCliente, string NumeroCuenta, double Saldo)
        {
            this.NombreCliente = NombreCliente;
            this.NumeroCuenta = NumeroCuenta;

            Movimientos = new List<Movimiento>();
            Depositar(Saldo, "Saldo Inicial");
        }

        public void Depositar(double Monto, string Concepto)
        {
            Movimientos.Add(new Movimiento(Monto, Concepto, 0));
        }

        public void Retirar(double Monto, string Concepto)
        {
            Movimientos.Add(new Movimiento(Monto, Concepto, 1));
        }

        public string GetDatosMovimientos()
        {
            string datos = "";
            for (int i = 0; i < Movimientos.Count; i++)
                datos += Movimientos[i].GetDatos() + "\n";
            return datos;

        }

        public double GetSaldo()
        {
            Saldo = 0;

            for (int i = 0; i < Movimientos.Count; i++)
                if (Movimientos[i].GetTipo() == 0)
                    Saldo += Movimientos[i].GetMonto();
                else
                    Saldo -= Movimientos[i].GetMonto();


            return Saldo;
        }
    }
}
