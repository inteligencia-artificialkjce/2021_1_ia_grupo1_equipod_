﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kmedias
{
    class Cluster
    {
        public string nombre;
        public List<float> coordenadas = new List<float>();

        public Cluster() //Constructor default para el cluster principal
        {

        }

        public Cluster(string coordenadas) //Constructor para cuando se crean Cluster leeyendo archivos
        {
            string[] coordenadaSeparada = coordenadas.Split(','); //Se separa la información por comas
            this.nombre = coordenadaSeparada[0]; //El primer valor se guarda como el nombre
            coordenadaSeparada = coordenadaSeparada.Skip(1).ToArray(); //El resto de valores después del 0 se  almacena en la misma variable

            for (int i = 0; i < coordenadaSeparada.Length; i++) //Se recorre cada valor de la variable 'coordenadaSeparada'
            {
                float dato = float.Parse(coordenadaSeparada[i]); //Ese dato se convierte a tipo 'float'
                this.coordenadas.Add(dato); //Se añade a nuestra lista de coordenadas
            }

        }
    }
}
