﻿
namespace Kmedias
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.centroidesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargarCentroidesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearCentroidesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.menuStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.centroidesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(73, 24);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // centroidesToolStripMenuItem
            // 
            this.centroidesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cargarCentroidesToolStripMenuItem,
            this.crearCentroidesToolStripMenuItem});
            this.centroidesToolStripMenuItem.Name = "centroidesToolStripMenuItem";
            this.centroidesToolStripMenuItem.Size = new System.Drawing.Size(94, 24);
            this.centroidesToolStripMenuItem.Text = "Centroides";
            // 
            // cargarCentroidesToolStripMenuItem
            // 
            this.cargarCentroidesToolStripMenuItem.Name = "cargarCentroidesToolStripMenuItem";
            this.cargarCentroidesToolStripMenuItem.Size = new System.Drawing.Size(211, 26);
            this.cargarCentroidesToolStripMenuItem.Text = "Cargar Centroides";
            // 
            // crearCentroidesToolStripMenuItem
            // 
            this.crearCentroidesToolStripMenuItem.Name = "crearCentroidesToolStripMenuItem";
            this.crearCentroidesToolStripMenuItem.Size = new System.Drawing.Size(211, 26);
            this.crearCentroidesToolStripMenuItem.Text = "Crear Centroides";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(12, 31);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(776, 407);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "KMedias";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem centroidesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cargarCentroidesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearCentroidesToolStripMenuItem;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}

