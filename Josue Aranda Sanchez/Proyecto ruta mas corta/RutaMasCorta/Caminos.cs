﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace RutaOptima
{
    class Proceso
    {
        List<Ruta> ways = new List<Ruta>(); 
        List<Ruta> rutasSeleccionadas = new List<Ruta>(); 
        List<string> estados = new List<string>(); 
        List<string[]> distCiudad = new List<string[]>(); 
        int numCiudad; 
        Ruta camino; 

        public void crearRutas(TextBox TBNCaminos)
        {
            Ruta newRuta;
            Random aleatorio = new Random();
            int numerocaminos = int.Parse(TBNCaminos.Text);

            for (int i=0; i<numerocaminos; i++)
            {
                newRuta=new Ruta(aleatorio, numCiudad); 
                ways.Add(newRuta); 
            }
        }

        public void calDist()
        {
            foreach (Ruta camino in ways) 
            {
                string[] ruta_Vector=camino.ruta.Split('.'); 

                int puntoA; 
                int puntoB;

                decimal distPosiciones = 0; 

                for (int k=0;k<ruta_Vector.Length-1;k++) 
                {
                    int indiceB = k + 1; 
                    puntoA=int.Parse(ruta_Vector[k].ToString()); 
                    puntoB=int.Parse(ruta_Vector[indiceB].ToString());
                    
                    distPosiciones = distPosiciones+decimal.Parse(distCiudad[puntoA][puntoB]); 
                }

                camino.distancia = distPosiciones; 
            }            
        }
        
        public void aplicarSeleccion(TextBox TBPSurv)
        {
            ways = ways.OrderBy(camino => camino.distancia).ToList(); 

            int probSurv = int.Parse(TBPSurv.Text); 
            int numSurv = (ways.Count() * probSurv) / 100; 

            if (numSurv<=1)
            {
                MessageBox.Show("Intente con otros valores"); 
            }

            for (int i=0;i<numSurv;i++)
            {
                rutasSeleccionadas.Add(ways[i]); 
            }
            
        }

        public void crossRutas(Random nuevoCaracter)
        {
            int puntoA, puntoB, cross; 
            int longitud = ways[0].ruta.Split('.').Length; 

            for (int i = rutasSeleccionadas.Count; i < ways.Count; i++) 
            {
                bool iguales = true; 
                puntoA = nuevoCaracter.Next(0, rutasSeleccionadas.Count); 
                puntoB = nuevoCaracter.Next(0, rutasSeleccionadas.Count);
                cross = nuevoCaracter.Next(1, longitud-1); 

                while (iguales)
                {
                    if (puntoA==puntoB) 
                    {
                        puntoA = nuevoCaracter.Next(0, rutasSeleccionadas.Count);
                    }
                    else
                    {
                        iguales = false;
                    }
                }
                
                                camino = new Ruta(rutasSeleccionadas[puntoA], rutasSeleccionadas[puntoB], cross, nuevoCaracter, numCiudad);
                                rutasSeleccionadas.Add(camino);
            }

            for (int i = 0; i < ways.Count; i++)
            {
                ways[i] = rutasSeleccionadas[i];             }

            rutasSeleccionadas.Clear(); 

            calDist();                         
        }

        public void mut(Random mutar, TextBox TBMutar, ListBox res)
        {
            float probMut = float.Parse(TBMutar.Text);             
            foreach (Ruta camino in ways)             {
                string Final = "";                 string[] rutV=camino.ruta.Split('.');                 
                for (int j=0;j<rutV.Length;j++)                 {
                    int prob=mutar.Next(0,100); 
                    if (prob <= probMut) 
                    {
                        
                        int nuevoChar=mutar.Next(0,numCiudad); 
                        int posicionCaracter = Array.IndexOf(rutV, nuevoChar.ToString()); 

                        rutV[posicionCaracter]=rutV[j]; 
                        rutV[j]=nuevoChar.ToString(); 
                    }                   
                }

                for (int k=0; k<rutV.Length; k++) 
                {
                    if (k == rutV.Length-1) 
                    {
                        Final += rutV[k];
                    }
                    else
                    {
                        Final += rutV[k] + ".";
                    }
                }

                camino.ruta = Final; 

                
                
            }
        }

        public void mostrarCam(ListBox res)
        {
            res.Items.Clear(); 

            foreach (Ruta c in ways) 
            {
                res.Items.Add("Camino: "+ c.distancia+" ruta "+c.ruta); 
            }
        }

        public void mostrarSeleccion(ListBox res)
        {
            res.Items.Clear();

            foreach (Ruta c in rutasSeleccionadas)
            {
                res.Items.Add("Camino: " + c.distancia + " ruta " + c.ruta); 
            }
        }

        public void mostrarOptimos(ListBox resultados)
        {
            string est = ""; 
            ways = ways.OrderBy(camino => camino.distancia).ToList(); 
            string[] estados_Vector = ways[0].ruta.Split('.'); 

            for (int k=0; k<estados_Vector.Length; k++) 
            {
                if (k == estados_Vector.Length-1) 
                {
                    est += estados[int.Parse(estados_Vector[k])];
                }
                else
                {
                    est += estados[int.Parse(estados_Vector[k])] + ", ";
                }
                
            }

            resultados.Items.Add("La ruta mas óptima es " + est + " y su fitness es de " + ways[0].distancia); 
        }

        public void leerDoc()
        {
            string direccion = "../../../01.csv";
            //string direccion = "../../../02.csv"; 
            //string direccion = "../../../03.csv"; 

            using (StreamReader reader=new StreamReader(direccion)) 
            {
                int cont=0; 
                string line; 
                string[] row; 
                
                while ((line=reader.ReadLine())!=null) 
                {
                    row = line.Split(','); 

                    if (cont==0) 
                    {
                        for (int j=0;j<row.Length;j++) 
                        {
                            if (row[j]!=" ") 
                            {
                                estados.Add(row[j]); 
                            }
                                
                        }
                    }

                    else 
                    {
                        string[] nuevaLinea=new string[row.Length-1]; 

                        for (int k=1;k<row.Length;k++)
                        {
                            nuevaLinea[k-1]=row[k]; 
                        }

                        distCiudad.Add(nuevaLinea); 
                    }                     

                    cont++; 
                }

                numCiudad=estados.Count(); 
            }
        }

    }
}
