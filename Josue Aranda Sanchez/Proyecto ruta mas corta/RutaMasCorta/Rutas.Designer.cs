﻿namespace RutaOptima
{
    partial class Rutas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.noIndiv = new System.Windows.Forms.Label();
            this.noIteraciones = new System.Windows.Forms.Label();
            this.probSobrevivir = new System.Windows.Forms.Label();
            this.probMutar = new System.Windows.Forms.Label();
            this.textBoxCaminos = new System.Windows.Forms.TextBox();
            this.textBoxIteraciones = new System.Windows.Forms.TextBox();
            this.textBoxprobSobrevivir = new System.Windows.Forms.TextBox();
            this.textBoxMutar = new System.Windows.Forms.TextBox();
            this.Resultados = new System.Windows.Forms.ListBox();
            this.resultado = new System.Windows.Forms.Label();
            this.buttonIniciar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // noIndiv
            // 
            this.noIndiv.AutoSize = true;
            this.noIndiv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noIndiv.Location = new System.Drawing.Point(16, 26);
            this.noIndiv.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.noIndiv.Name = "noIndiv";
            this.noIndiv.Size = new System.Drawing.Size(84, 17);
            this.noIndiv.TabIndex = 0;
            this.noIndiv.Text = "Población:";
            // 
            // noIteraciones
            // 
            this.noIteraciones.AutoSize = true;
            this.noIteraciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noIteraciones.Location = new System.Drawing.Point(16, 74);
            this.noIteraciones.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.noIteraciones.Name = "noIteraciones";
            this.noIteraciones.Size = new System.Drawing.Size(93, 17);
            this.noIteraciones.TabIndex = 1;
            this.noIteraciones.Text = "Iteraciones:";
            this.noIteraciones.Click += new System.EventHandler(this.label1_Click);
            // 
            // probSobrevivir
            // 
            this.probSobrevivir.AutoSize = true;
            this.probSobrevivir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.probSobrevivir.Location = new System.Drawing.Point(16, 122);
            this.probSobrevivir.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.probSobrevivir.Name = "probSobrevivir";
            this.probSobrevivir.Size = new System.Drawing.Size(83, 17);
            this.probSobrevivir.TabIndex = 2;
            this.probSobrevivir.Text = "Selección:";
            this.probSobrevivir.Click += new System.EventHandler(this.probSobrevivir_Click);
            // 
            // probMutar
            // 
            this.probMutar.AutoSize = true;
            this.probMutar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.probMutar.Location = new System.Drawing.Point(16, 169);
            this.probMutar.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.probMutar.Name = "probMutar";
            this.probMutar.Size = new System.Drawing.Size(78, 17);
            this.probMutar.TabIndex = 3;
            this.probMutar.Text = "Mutacion:";
            // 
            // textBoxCaminos
            // 
            this.textBoxCaminos.Location = new System.Drawing.Point(141, 26);
            this.textBoxCaminos.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCaminos.Name = "textBoxCaminos";
            this.textBoxCaminos.Size = new System.Drawing.Size(87, 22);
            this.textBoxCaminos.TabIndex = 4;
            // 
            // textBoxIteraciones
            // 
            this.textBoxIteraciones.Location = new System.Drawing.Point(141, 74);
            this.textBoxIteraciones.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxIteraciones.Name = "textBoxIteraciones";
            this.textBoxIteraciones.Size = new System.Drawing.Size(87, 22);
            this.textBoxIteraciones.TabIndex = 5;
            // 
            // textBoxprobSobrevivir
            // 
            this.textBoxprobSobrevivir.Location = new System.Drawing.Point(141, 122);
            this.textBoxprobSobrevivir.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxprobSobrevivir.Name = "textBoxprobSobrevivir";
            this.textBoxprobSobrevivir.Size = new System.Drawing.Size(87, 22);
            this.textBoxprobSobrevivir.TabIndex = 6;
            this.textBoxprobSobrevivir.TextChanged += new System.EventHandler(this.textBoxprobSobrevivir_TextChanged);
            // 
            // textBoxMutar
            // 
            this.textBoxMutar.Location = new System.Drawing.Point(141, 169);
            this.textBoxMutar.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxMutar.Name = "textBoxMutar";
            this.textBoxMutar.Size = new System.Drawing.Size(87, 22);
            this.textBoxMutar.TabIndex = 7;
            // 
            // Resultados
            // 
            this.Resultados.FormattingEnabled = true;
            this.Resultados.HorizontalScrollbar = true;
            this.Resultados.ItemHeight = 16;
            this.Resultados.Location = new System.Drawing.Point(257, 13);
            this.Resultados.Margin = new System.Windows.Forms.Padding(4);
            this.Resultados.Name = "Resultados";
            this.Resultados.Size = new System.Drawing.Size(636, 356);
            this.Resultados.TabIndex = 8;
            this.Resultados.SelectedIndexChanged += new System.EventHandler(this.listBoxResultados_SelectedIndexChanged);
            // 
            // resultado
            // 
            this.resultado.AutoSize = true;
            this.resultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultado.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.resultado.Location = new System.Drawing.Point(373, 119);
            this.resultado.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.resultado.Name = "resultado";
            this.resultado.Size = new System.Drawing.Size(0, 17);
            this.resultado.TabIndex = 9;
            this.resultado.Click += new System.EventHandler(this.resultado_Click);
            // 
            // buttonIniciar
            // 
            this.buttonIniciar.ForeColor = System.Drawing.Color.Red;
            this.buttonIniciar.Location = new System.Drawing.Point(141, 209);
            this.buttonIniciar.Margin = new System.Windows.Forms.Padding(4);
            this.buttonIniciar.Name = "buttonIniciar";
            this.buttonIniciar.Size = new System.Drawing.Size(87, 38);
            this.buttonIniciar.TabIndex = 10;
            this.buttonIniciar.Text = "Iniciar";
            this.buttonIniciar.UseVisualStyleBackColor = true;
            this.buttonIniciar.Click += new System.EventHandler(this.BCIniciar);
            // 
            // Rutas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(906, 392);
            this.Controls.Add(this.buttonIniciar);
            this.Controls.Add(this.resultado);
            this.Controls.Add(this.Resultados);
            this.Controls.Add(this.textBoxMutar);
            this.Controls.Add(this.textBoxprobSobrevivir);
            this.Controls.Add(this.textBoxIteraciones);
            this.Controls.Add(this.textBoxCaminos);
            this.Controls.Add(this.probMutar);
            this.Controls.Add(this.probSobrevivir);
            this.Controls.Add(this.noIteraciones);
            this.Controls.Add(this.noIndiv);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Rutas";
            this.Text = "Rutas";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label noIndiv;
        private System.Windows.Forms.Label noIteraciones;
        private System.Windows.Forms.Label probSobrevivir;
        private System.Windows.Forms.Label probMutar;
        private System.Windows.Forms.TextBox textBoxCaminos;
        private System.Windows.Forms.TextBox textBoxIteraciones;
        private System.Windows.Forms.TextBox textBoxprobSobrevivir;
        private System.Windows.Forms.TextBox textBoxMutar;
        private System.Windows.Forms.ListBox Resultados;
        private System.Windows.Forms.Label resultado;
        private System.Windows.Forms.Button buttonIniciar;
    }
}

