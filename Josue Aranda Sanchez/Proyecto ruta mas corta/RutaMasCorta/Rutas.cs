﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RutaOptima
{
    public partial class Rutas : Form
    {
        Proceso proceso;
        Random random = new Random();
        int contadorIteraciones = 0; 

        public Rutas()
        {
            InitializeComponent();
            proceso = new Proceso();
            proceso.leerDoc();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void BCIniciar(object sender, EventArgs e)
        {
            while (contadorIteraciones < int.Parse(textBoxIteraciones.Text))
            {
                Resultados.Items.Add("Iteracion: " + (contadorIteraciones+1) +":");

                proceso.crearRutas(textBoxCaminos);
                proceso.calDist();
                proceso.aplicarSeleccion(textBoxprobSobrevivir);
                proceso.crossRutas(random);
                proceso.mut(random, textBoxMutar, Resultados);
                proceso.mostrarOptimos(Resultados);
                contadorIteraciones++;
            }
            
        }

        private void resultado_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void listBoxResultados_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void probSobrevivir_Click(object sender, EventArgs e)
        {

        }

        private void textBoxprobSobrevivir_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
