﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RutaOptima
{
    class Ruta
    {
        public string ruta=""; 
        public decimal distancia; 

        
        public Ruta(Random random, int ciudades)
        {
            int caracter;
            string[] camin;            

            for (int x=0;x<ciudades;x++)
            {
                bool existecaracter=true; 

                
                caracter=random.Next(0,ciudades);

                
                camin=ruta.Split('.');

                while (existecaracter) 
                {
                    if (camin.Contains(caracter.ToString()))
                    {
                        caracter=random.Next(0, ciudades);
                    }

                    else
                    {
                        existecaracter=false; 
                    }
                }
                
                if (x<ciudades-1)
                {
                    ruta = ruta+caracter+".";
                }
                else
                {
                    ruta = ruta+caracter; 
                }

            }
        }

       
        public Ruta(Ruta posA, Ruta posB, int cross, Random newChar, int ciudades)
        {
            string[] posicionA=posA.ruta.Split('.'); 
            string[] posicionB=posB.ruta.Split('.');
            string[] rutaV;
            string caracter;

            for (int i=0;i<posicionB.Length;i++)
            {
                rutaV=ruta.Split('.');
                bool existecaracter = true; 

                if (i>cross) 
                {
                    caracter=posicionA[i].ToString(); 

                    while (existecaracter) 
                    {
                        if (rutaV.Contains(caracter))
                        {
                            caracter = newChar.Next(0, ciudades).ToString(); 
                        }
                        else
                        {
                            existecaracter = false;
                        }
                    }

                    if (i!=posicionA.Length-1) 
                    {
                        ruta=ruta+caracter+".";
                    }

                    else
                    {
                        ruta=ruta+caracter;
                    }
                }

                else
                {
                    ruta=ruta+posicionB[i]+"."; 
                }
            }


        }
    }
}
