﻿
namespace EjemplOAlgoritmoGenetico
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.NUDTamPoblacion = new System.Windows.Forms.NumericUpDown();
            this.RTBPoblacion = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.RTBEvaluados = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TBModelo = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.RTBSelecionados = new System.Windows.Forms.RichTextBox();
            this.NUDPresion = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.RTBCruzados = new System.Windows.Forms.RichTextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.RTBMutados = new System.Windows.Forms.RichTextBox();
            this.NUDMutacion = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.NUDTamPoblacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDPresion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDMutacion)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tamaño de la poblacion";
            // 
            // NUDTamPoblacion
            // 
            this.NUDTamPoblacion.Location = new System.Drawing.Point(136, 16);
            this.NUDTamPoblacion.Name = "NUDTamPoblacion";
            this.NUDTamPoblacion.Size = new System.Drawing.Size(60, 20);
            this.NUDTamPoblacion.TabIndex = 1;
            this.NUDTamPoblacion.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // RTBPoblacion
            // 
            this.RTBPoblacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.RTBPoblacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RTBPoblacion.ForeColor = System.Drawing.SystemColors.WindowText;
            this.RTBPoblacion.Location = new System.Drawing.Point(12, 71);
            this.RTBPoblacion.Name = "RTBPoblacion";
            this.RTBPoblacion.ReadOnly = true;
            this.RTBPoblacion.Size = new System.Drawing.Size(194, 334);
            this.RTBPoblacion.TabIndex = 3;
            this.RTBPoblacion.Text = "";
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button1.Location = new System.Drawing.Point(65, 42);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Inicializar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(266, 42);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "Evaluar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // RTBEvaluados
            // 
            this.RTBEvaluados.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.RTBEvaluados.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RTBEvaluados.ForeColor = System.Drawing.SystemColors.WindowText;
            this.RTBEvaluados.Location = new System.Drawing.Point(212, 71);
            this.RTBEvaluados.Name = "RTBEvaluados";
            this.RTBEvaluados.ReadOnly = true;
            this.RTBEvaluados.Size = new System.Drawing.Size(159, 334);
            this.RTBEvaluados.TabIndex = 7;
            this.RTBEvaluados.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(209, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Modelo";
            // 
            // TBModelo
            // 
            this.TBModelo.Location = new System.Drawing.Point(266, 16);
            this.TBModelo.Name = "TBModelo";
            this.TBModelo.Size = new System.Drawing.Size(88, 20);
            this.TBModelo.TabIndex = 9;
            this.TBModelo.Text = "12345678";
            this.TBModelo.Validating += new System.ComponentModel.CancelEventHandler(this.TBModelo_Validating);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(425, 42);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 13;
            this.button3.Text = "Seleccionar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // RTBSelecionados
            // 
            this.RTBSelecionados.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.RTBSelecionados.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RTBSelecionados.ForeColor = System.Drawing.SystemColors.WindowText;
            this.RTBSelecionados.Location = new System.Drawing.Point(377, 71);
            this.RTBSelecionados.Name = "RTBSelecionados";
            this.RTBSelecionados.ReadOnly = true;
            this.RTBSelecionados.Size = new System.Drawing.Size(160, 334);
            this.RTBSelecionados.TabIndex = 12;
            this.RTBSelecionados.Text = "";
            // 
            // NUDPresion
            // 
            this.NUDPresion.Location = new System.Drawing.Point(440, 17);
            this.NUDPresion.Name = "NUDPresion";
            this.NUDPresion.Size = new System.Drawing.Size(60, 20);
            this.NUDPresion.TabIndex = 11;
            this.NUDPresion.Value = new decimal(new int[] {
            70,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(374, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Presion";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(582, 42);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 15;
            this.button4.Text = "Cruzar";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // RTBCruzados
            // 
            this.RTBCruzados.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.RTBCruzados.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RTBCruzados.ForeColor = System.Drawing.SystemColors.WindowText;
            this.RTBCruzados.Location = new System.Drawing.Point(543, 71);
            this.RTBCruzados.Name = "RTBCruzados";
            this.RTBCruzados.ReadOnly = true;
            this.RTBCruzados.Size = new System.Drawing.Size(160, 334);
            this.RTBCruzados.TabIndex = 14;
            this.RTBCruzados.Text = "";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(757, 42);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 19;
            this.button5.Text = "Mutar";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // RTBMutados
            // 
            this.RTBMutados.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.RTBMutados.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RTBMutados.ForeColor = System.Drawing.SystemColors.WindowText;
            this.RTBMutados.Location = new System.Drawing.Point(709, 71);
            this.RTBMutados.Name = "RTBMutados";
            this.RTBMutados.ReadOnly = true;
            this.RTBMutados.Size = new System.Drawing.Size(160, 334);
            this.RTBMutados.TabIndex = 18;
            this.RTBMutados.Text = "";
            // 
            // NUDMutacion
            // 
            this.NUDMutacion.Location = new System.Drawing.Point(838, 21);
            this.NUDMutacion.Name = "NUDMutacion";
            this.NUDMutacion.Size = new System.Drawing.Size(60, 20);
            this.NUDMutacion.TabIndex = 17;
            this.NUDMutacion.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(706, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Probabilidad de mutacion";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(917, 417);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.RTBMutados);
            this.Controls.Add(this.NUDMutacion);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.RTBCruzados);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.RTBSelecionados);
            this.Controls.Add(this.NUDPresion);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TBModelo);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.RTBEvaluados);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.RTBPoblacion);
            this.Controls.Add(this.NUDTamPoblacion);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.NUDTamPoblacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDPresion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDMutacion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown NUDTamPoblacion;
        private System.Windows.Forms.RichTextBox RTBPoblacion;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RichTextBox RTBEvaluados;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TBModelo;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.RichTextBox RTBSelecionados;
        private System.Windows.Forms.NumericUpDown NUDPresion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.RichTextBox RTBCruzados;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.RichTextBox RTBMutados;
        private System.Windows.Forms.NumericUpDown NUDMutacion;
        private System.Windows.Forms.Label label4;
    }
}

