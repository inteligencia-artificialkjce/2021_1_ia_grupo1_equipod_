﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemplOAlgoritmoGenetico
{
    class Individuo
    {
        string Genotipo;
        public int Fitness { get; private set; }


        public Individuo(Random Aleatorio)
        {
            Genotipo = "";

            for (int i = 0; i < 8; i++)
                Genotipo += Aleatorio.Next(1, 8);
        }


        public Individuo(Individuo Padre, Individuo Madre, int PuntoDeCruce)
        {
            Genotipo = Padre.Genotipo.Substring(0, PuntoDeCruce);
            Genotipo += Madre.Genotipo.Substring(PuntoDeCruce);
        }
        
     public int Evaluacion(string Modelo)
        {
            Fitness = 0;
            for (int i = 0; i < Genotipo.Length; i++)
                if (Genotipo[i] == Modelo[i])
                    Fitness++;
            return Fitness;
        }

        public void Muta(int ProbabilidadMutacion, Random Aleatorio)
        {
            string genotipo_aux = "";

            for(int i =0; i< Genotipo.Length; i++)
            {
                if (Aleatorio.Next(0, 100) < ProbabilidadMutacion)
                    genotipo_aux += Aleatorio.Next(1, 8);
                else
                    genotipo_aux += Genotipo[i];
            }

            Genotipo = genotipo_aux;

        }
        public string GetDatos()
        {
            string datos = "";

            datos += Genotipo + " - " +Fitness;

            return datos;
        }
    }
}
