﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemplOAlgoritmoGenetico
{
    class Poblacion
    {
        List<Individuo> Individuos;
        Random Aleatorio;
        int TamPoblacion;
        int NSeleccionados;

        public Poblacion(int TamPoblacion)
        {
            Individuos = new List<Individuo>();

            this.TamPoblacion = TamPoblacion;

            Aleatorio = new Random();

            for (int i = 0; i < TamPoblacion; i++)
                Individuos.Add(new Individuo(Aleatorio));
        }

        public void Evaluacion(string Modelo)
        {
            for (int i = 0; i < Individuos.Count; i++)
                Individuos[i].Evaluacion(Modelo);
        }

        public void Seleccion(int Presion)
        {
            Individuo aux;

            for (int i = 0; i < Individuos.Count - 1; i++)
                for (int j = i + 1; j < Individuos.Count; j++)
                    if (Individuos[i].Fitness < Individuos[j].Fitness)
                    {
                        aux = Individuos[i];
                        Individuos[i] = Individuos[j];
                        Individuos[j] = aux;
                    }

            NSeleccionados = TamPoblacion * (100 - Presion) / 100;

            while (Individuos.Count > NSeleccionados)
                Individuos.RemoveAt(Individuos.Count - 1);
            
        }

        public void Cruzamiento()
        {
            Individuo padre;
            Individuo madre;
            Individuo hijo;
            int valor_aux;


            for (int i = NSeleccionados; i < TamPoblacion; i++)
            {
                valor_aux = Aleatorio.Next(0, NSeleccionados - 1);
                padre = Individuos[valor_aux];

                valor_aux = Aleatorio.Next(0, NSeleccionados - 1);
                madre = Individuos[valor_aux];

                valor_aux = Aleatorio.Next(0, 8);

                hijo = new Individuo(padre, madre, valor_aux);
                Individuos.Add(hijo);
            }
        }


        public void Mutacion (int ProbabilidadMutacion)
        {
            for (int i = NSeleccionados; i < Individuos.Count; i++)
                Individuos[i].Muta(ProbabilidadMutacion, Aleatorio);
        }

        public string GetDatosPoblacion()
        {
            string datos = "";

            for (int i = 0; i < Individuos.Count; i++)
                datos += Individuos[i].GetDatos() + "\n";
            
            return datos;
        }
    }
}
