﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepasoPOO
{
    class CuentaBancaria
    {
        string NombreCliente;
        string NumeroCuenta;
        double Saldo;

        public CuentaBancaria(string NombreCliente, string NumeroCuenta, double Saldo)
        {
            this.NombreCliente = NombreCliente;
            this.NumeroCuenta = NumeroCuenta;
            this.Saldo = Saldo;
        }

        public void Depositar(double Monto)
        {
            Saldo += Monto;
        }

        public void Retirar(double Monto)
        {
            Saldo -= Monto;
        }
    }
}
