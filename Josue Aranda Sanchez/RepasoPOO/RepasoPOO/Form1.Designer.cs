﻿namespace RepasoPOO
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TBNombre = new System.Windows.Forms.TextBox();
            this.TBNumdeCuenta = new System.Windows.Forms.TextBox();
            this.NUDSaldo = new System.Windows.Forms.NumericUpDown();
            this.BAceptar = new System.Windows.Forms.Button();
            this.BCancelar = new System.Windows.Forms.Button();
            this.BDepositar = new System.Windows.Forms.Button();
            this.BRetirar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.NUDDEpositar = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.NUDRetirar = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.NUDSaldo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDDEpositar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDRetirar)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Numero de cuenta:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Saldo:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // TBNombre
            // 
            this.TBNombre.Location = new System.Drawing.Point(165, 3);
            this.TBNombre.Name = "TBNombre";
            this.TBNombre.Size = new System.Drawing.Size(189, 22);
            this.TBNombre.TabIndex = 3;
            // 
            // TBNumdeCuenta
            // 
            this.TBNumdeCuenta.Location = new System.Drawing.Point(165, 32);
            this.TBNumdeCuenta.Name = "TBNumdeCuenta";
            this.TBNumdeCuenta.Size = new System.Drawing.Size(189, 22);
            this.TBNumdeCuenta.TabIndex = 4;
            // 
            // NUDSaldo
            // 
            this.NUDSaldo.Location = new System.Drawing.Point(165, 64);
            this.NUDSaldo.Name = "NUDSaldo";
            this.NUDSaldo.Size = new System.Drawing.Size(189, 22);
            this.NUDSaldo.TabIndex = 5;
            // 
            // BAceptar
            // 
            this.BAceptar.Location = new System.Drawing.Point(165, 106);
            this.BAceptar.Name = "BAceptar";
            this.BAceptar.Size = new System.Drawing.Size(75, 23);
            this.BAceptar.TabIndex = 6;
            this.BAceptar.Text = "Aceptar";
            this.BAceptar.UseVisualStyleBackColor = true;
            this.BAceptar.Click += new System.EventHandler(this.BAceptar_Click);
            // 
            // BCancelar
            // 
            this.BCancelar.Location = new System.Drawing.Point(279, 106);
            this.BCancelar.Name = "BCancelar";
            this.BCancelar.Size = new System.Drawing.Size(75, 23);
            this.BCancelar.TabIndex = 7;
            this.BCancelar.Text = "Cancelar";
            this.BCancelar.UseVisualStyleBackColor = true;
            // 
            // BDepositar
            // 
            this.BDepositar.Location = new System.Drawing.Point(377, 187);
            this.BDepositar.Name = "BDepositar";
            this.BDepositar.Size = new System.Drawing.Size(75, 23);
            this.BDepositar.TabIndex = 8;
            this.BDepositar.Text = "Depositar";
            this.BDepositar.UseVisualStyleBackColor = true;
            // 
            // BRetirar
            // 
            this.BRetirar.Location = new System.Drawing.Point(377, 235);
            this.BRetirar.Name = "BRetirar";
            this.BRetirar.Size = new System.Drawing.Size(75, 23);
            this.BRetirar.TabIndex = 9;
            this.BRetirar.Text = "Retirar";
            this.BRetirar.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 187);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(139, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Cantidad a depositar";
            // 
            // NUDDEpositar
            // 
            this.NUDDEpositar.Location = new System.Drawing.Point(165, 187);
            this.NUDDEpositar.Name = "NUDDEpositar";
            this.NUDDEpositar.Size = new System.Drawing.Size(189, 22);
            this.NUDDEpositar.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 238);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "Cantidad a retirar";
            // 
            // NUDRetirar
            // 
            this.NUDRetirar.Location = new System.Drawing.Point(165, 238);
            this.NUDRetirar.Name = "NUDRetirar";
            this.NUDRetirar.Size = new System.Drawing.Size(189, 22);
            this.NUDRetirar.TabIndex = 13;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(807, 450);
            this.Controls.Add(this.NUDRetirar);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.NUDDEpositar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.BRetirar);
            this.Controls.Add(this.BDepositar);
            this.Controls.Add(this.BCancelar);
            this.Controls.Add(this.BAceptar);
            this.Controls.Add(this.NUDSaldo);
            this.Controls.Add(this.TBNumdeCuenta);
            this.Controls.Add(this.TBNombre);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.NUDSaldo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDDEpositar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDRetirar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TBNombre;
        private System.Windows.Forms.TextBox TBNumdeCuenta;
        private System.Windows.Forms.NumericUpDown NUDSaldo;
        private System.Windows.Forms.Button BAceptar;
        private System.Windows.Forms.Button BCancelar;
        private System.Windows.Forms.Button BDepositar;
        private System.Windows.Forms.Button BRetirar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown NUDDEpositar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown NUDRetirar;
    }
}

